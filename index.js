console.log("Hello World");

// [SECTION] While Loop
	// A while loop takes in an expression/condition/
	// Expression/s is/are any unit of code that can be evaluated to a value.
	// If the condition evaluates to be true, the statements inside the code block will be executed.
	// A loop will iterate a certain number of times until an expression or condition is met.
	// Iteration is the term given to the repetition of statements. 

	/*
		Syntax:
			while(expression/condition) {
				statements;
				increment/decrement;
			}
	*/

	let count = 5;

		while(count !== 0) {
			console.log("The current value of count is " + count);
			// decrement
			count--;

			// Decreases the value of count by 1 after every iteration to stop the loop when it reaches 0.
			// Loops occupy a significant amount of memory spaces in our devices.
			// Reminder: Make sure the expression/condition in the loops have their corresponding increment/decrement operators to stop the loop.
		}


// [SECTION] Do While Loop
	// a do-while loop works like a while loop but unlike while loops, do-while loops guarantee that the code will be executed at least once.

	/*
		Syntax:
			do {
				statements;
				increment/decrement;

			}while(expression/conditions)
	*/

	// Number() - converts the input of the user from string to a number.
	let number = Number(prompt("Give me a number:"));

	do{
		console.log("Do while: " + number);
		number++;
	}while(number < 10)


// [SECTION] For Loop
	/*
		- For Loop is more flexible than while and do-while.
		- It consists of 3 parts:
			1. "Initialization" value that will track the progression of the loop
			2. The expressiion/condition that will be evaluated which will determine whether the loop will run one more time.
			3. the "Final Expression" indicates how the loop will advance.

		Syntax:
			for(initialization; expression/condition; finalExpression) {
				statement/s;
			}

	*/

	// Business Logic
		// 1. We will create a loop that will start from zero and end at 20.
		// 2. Every iteration of the loop, the value of the count will be checked if it is equal or less than 20
		// 3. If the value of count is less than or equal to 20, the statement inside the loop will run.
		// 4. The value of count will be incremented by one for each iteration.

	for(let count = 0; count <= 20; count++) {
		console.log('The current value of count is ' + count);
	}

	let myString = "Christopher";
		// character in strings can be counted using the .length property
	console.log(myString.length);

	// Accessing the characters of a string, you can also use the index of the letter/character.
	console.log(myString[myString.length - 1]);

	let myName = "John Edward"
	/*
		- Create a loop that will print out the letters of our name individually.

		- Console number 3 instead when the letter to be printed out is vowel
	*/

	for(let i = 0; i < myName.length; i++) {
		console.log(myName[i]);
	}

	for(let i = 0; i < myName.length; i++) {
		myName = myName.toLowerCase();
		if(myName[i] === "a" ||
			myName[i] === "e" ||
			myName[i] === "i" ||
			myName[i] === "o" ||
			myName[i] === "u"){
			console.log(3);

		}
		else {
			console.log(myName[i]);
		}
	}


// [SECTION] Continue and Break statements
	/*
		- The "Continue" statement allows the code to go to the next iteration of the loop without finishing the execution of all statements in a code block.

		- The "Break" statement is used to terminate the current loop once a match has been found.
	*/

	for(let count = 0; count <=20; count++) {
		// if we divide count to 2 and if the remainder is equal to 0
		if(count % 2 === 0) {
			// tells the code to continue to the next iteration of the loop
			// this also ignores all statements located after the continue statement
			continue;
		}

		console.log("Continue and Break: " + count);

		if(count > 10) {
			// tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20.
			break;
		}
	}

	/*
	- Creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
    - How this For Loop works:
        1. The loop will start at 0 for the the value of "count".
        2. It will check if "count" is less than the or equal to 20.
        3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2).
        4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
        5. If the value of count is not equal to 0, the console will print the value of "count".
        6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
        7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
        8. The value of "count" will be incremented by 1 (e.g. count = 1)
        9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] > 10) is true, the loop will stop due to the "break" statement
        */

